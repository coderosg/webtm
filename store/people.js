export const state = () => ({
  peopleList: {},
  detail: null,
})

export const mutations = {
  SET_PEOPLE_LIST(state, payload) {
    state.peopleList = payload
  },
  SET_PEOPLE_DETAIL(state, payload) {
    state.detail = payload
  },
}

export const actions = {
  getPeopleListAction({ commit }, params) {
    const xhrs = []
    commit('SET_PEOPLE_LIST', [])
    return this.$axios.get('people/', { params }).then((response) => {
      response.data.results.forEach((people) => {
        people.starships.forEach((item, index, arr) => {
          const apiEndpoint = item.replace(/^http?:\/\//i, 'https://')
          const apiQuery = this.$axios.get(apiEndpoint).then((res) => {
            arr[index] = res.data
          })
          xhrs.push(apiQuery)
        })
      })
      Promise.all(xhrs).finally(() => {
        commit('SET_PEOPLE_LIST', response.data)
      })
    })
  },
  getPeopleDetailAction({ commit }, id) {
    const xhrs = []
    commit('SET_PEOPLE_DETAIL', null)
    return this.$axios.get(`people/${id}/`).then((response) => {
      response.data.films.forEach((item, index, arr) => {
        const apiEndpoint = item.replace(/^http?:\/\//i, 'https://')
        const apiQuery = this.$axios.get(apiEndpoint).then((res) => {
          arr[index] = res.data
        })
        xhrs.push(apiQuery)
      })
      Promise.all(xhrs).finally(() => {
        commit('SET_PEOPLE_DETAIL', response.data)
      })
    })
  },
}

export const getters = {}
