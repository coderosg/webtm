import { createLogger } from 'vuex'

export const plugins =
  process.env.NODE_ENV !== 'production' ? [createLogger()] : []
