import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import PeopleCard from '@/components/pages/people/PeopleCard'
import PeopleDetail from '@/pages/people/_id'

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter({
  routes: [
    {
      name: 'people-id',
      path: '/people/:id/',
      component: PeopleDetail,
    },
  ],
})

describe('PeopleCard', () => {
  let detail
  beforeEach(() => {
    detail = {
      name: 'R2-D2',
      height: '96',
      mass: '32',
      hair_color: 'n/a',
      skin_color: 'white, blue',
      eye_color: 'red',
      birth_year: '33BBY',
      gender: 'n/a',
      vehicles: [],
      starships: [],
      created: '2014-12-10T15:11:50.376000Z',
      edited: '2014-12-20T21:17:50.311000Z',
      url: 'http://swapi.dev/api/people/3/',
    }
  })
  test('is a Vue instance', () => {
    const wrapper = mount(PeopleCard, {
      propsData: {
        detail,
      },
      localVue,
      router,
    })
    expect(wrapper.vm).toBeTruthy()
  })

  test('correct rendered', () => {
    const wrapper = mount(PeopleCard, {
      propsData: {
        detail,
      },
      localVue,
      router,
    })
    const name = wrapper.find('div[data-test="name"]')
    const height = wrapper.find('p[data-test="height"]')
    const mass = wrapper.find('p[data-test="mass"]')
    expect(name.text()).toContain(detail.name)
    expect(height.text()).toContain(detail.height)
    expect(mass.text()).toContain(detail.mass)
  })
})
